# OneDriveApp

The OneDriveApp is a simple application to access oneDrive using Microsoft Graph API allowing upload and download files.
It is written over C# using Windows Form for .Net Framework 4.5



## Links

* Project Page - <https://bitbucket.org/Michel-braz-morais/onedriveapp>
* Microsoft Graph API - <https://developer.microsoft.com/en-us/graph>


## Libraries

The application uses the following libs:

* Microsoft.Graph.1.2.1         <https://www.nuget.org/packages/Microsoft.Graph> Microsoft Graph .NET Client Library

* Microsoft.Identity.Client     <https://www.nuget.org/packages/Microsoft.Identity.Client> Microsoft Authentication Library (MSAL).

* MaterialSkin 0.2.2            <https://www.nuget.org/packages/MaterialSkin.Updated> Theming WinForms (C# or VB.Net) to Google's Material Design Principles.

* Newtonsoft.Json.9.0.1         <https://www.nuget.org/packages/newtonsoft.json> Json.NET is a popular high-performance JSON framework for .NET.



## Authors

  - Michel Braz de Morais <michel.braz.morais@gmail.com>
  

## Getting started

### Register your application
Register your application to use Microsoft Graph API using one of the following supported authentication portal:

https://apps.dev.microsoft.com: Register your application that works with Microsoft Account account using the unified V2 Authentication Endpoint.

Make sure that you have added the following permission:

- Files.ReadWrite.All

### Get Your MSA Client ID and put on your code
```csharp
    ...
    private const string MsaClientId = "12cb44b2-000-000-fff-89197d0b0e00"; //This is an example of MSA id
    ...
```
Now you should be able to run.

### How to interact with yours files on oneDriveApp

Enter your outlook account as you can see bellow.

![Scheme](screen_shot/screen-1.png)

### Grant access to the application

![Scheme](screen_shot/screen-2.png)


### Screenshot from application

Now you are able to download file, upload file, delete item (file or folder), create folder and navigate into your onedrive account:

![Scheme](screen_shot/screen-3.png)


## License

OneDriveApp is under MIT License and the proposal is a simple access to oneDrive from Microsoft using Graph API.

The MIT License (MIT)

Copyright (c) 2018 [Michel Braz de Morais](michel.braz.morais@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sub license, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

        
          