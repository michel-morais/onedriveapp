﻿using Microsoft.Graph;
using Microsoft.Identity.Client;
using OneDriveApp.Form;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace OneDriveApp.Class
{
    public abstract class IOneDriveConnect : IException
    {
        #region private_variables

        private readonly string[] Scopes;
        private readonly string expandString = "children";
        private string TokenForUser = null;
        private PublicClientApplication IdentityClientApp;
        private DateTimeOffset Expiration;
        private Dictionary<string, DriveItem> MapFolder;
        private GraphServiceClient graphClient = null;
        private DriveItem CurrentFolder = null;
        private DriveItem InnerFolder = null;
        private bool IsLogged;
        #endregion

        #region public_methods

        public IOneDriveConnect(string MsaClientId,string[] scopes)
        {
            this.Scopes = scopes;
            this.IdentityClientApp = new PublicClientApplication(MsaClientId);
            this.MapFolder = new Dictionary<string, DriveItem>();
            this.IsLogged = false;
        }

        public class FileDetail
        {
            public readonly string FileName;
            public readonly string Id;
            public readonly long FileSize;
            public readonly bool IsFolder;
            public FileDetail(string id, string fileName, long size, bool isFolder)
            {
                Id = id;
                FileName = fileName;
                FileSize = size;
                IsFolder = isFolder;
            }
            public string GetHumanSizeFile()
            {
                string[] sizes = { "Bytes", "KB", "MB", "GB", "TB" };
                int order = 0;
                long size = FileSize;
                while (size >= 1024 && order < sizes.Length - 1)
                {
                    order++;
                    size = size / 1024;
                }
                string result = String.Format("{0:0.##} {1}", size, sizes[order]);
                return result;
            }
        }

        public string GetConnectedNameUser()
        {
            if (IdentityClientApp.Users != null)
            {
                var who = IdentityClientApp.Users.FirstOrDefault();
                if (who != null)
                    return who.Name;
            }
            return null;
        }

        public async Task<List<FileDetail>> ListFilesOnFolder(FileDetail Detail)
        {
            bool bNeedReloadFolder = true;
            if(Detail == null && CurrentFolder != null && CurrentFolder.Name == "root")
            {
                bNeedReloadFolder = false;
            }
            else if (CurrentFolder != null && Detail != null && CurrentFolder.Name == Detail.FileName)
            {
                bNeedReloadFolder = false;
            }
            if (bNeedReloadFolder)
            {
                if(Detail != null)
                {
                    if (await LoadFolderFromPathOrId(Detail.Id,true) == false)
                        return null;
                }
                else if (await LoadFolderFromPathOrId(null) == false)
                    return null;
            }
            List<FileDetail> listFiles = new List<FileDetail>();
            foreach(KeyValuePair<string, DriveItem> entry in MapFolder)
            {
                DriveItem Item = entry.Value;
                listFiles.Add(new FileDetail(Item.Id,Item.Name, (long)Item.Size, Item.Folder != null));
            }
            return listFiles;
        }

        public async Task<bool> SignIn()
        {
            try
            {
                if (this.graphClient == null)
                {
                    this.graphClient = new GraphServiceClient(
                        "https://graph.microsoft.com/v1.0",
                        new DelegateAuthenticationProvider(
                            async (requestMessage) =>
                            {
                                var token = await GetTokenForUserAsync();
                                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("bearer", token);
                            }));
                }
            }
            catch (ServiceException exception)
            {
                ShowDialogException(exception);
                this.graphClient = null;
                return false;
            }
            try
            {
                if (await LoadFolderFromPathOrId() == false)
                    return false;
                this.IsLogged = true;
                return true;
            }
            catch (ServiceException exception)
            {
                ShowDialogException(exception);
                this.graphClient = null;
                return false;
            }
        }

        public void SignOut()
        {
            this.IsLogged = false;
            foreach (var user in IdentityClientApp.Users)
            {
                user.SignOut();
            }
            TokenForUser = null;
        }

        public FileDetail GetDriveItem(string NameInCurrentFolder)
        {
            try
            {
                var Item = MapFolder[NameInCurrentFolder];
                return new FileDetail(Item.Id,Item.Name, (long)Item.Size, Item.Folder != null);
            }
            catch(Exception ex)
            {
                ShowDialogException(ex);
                return null;
            }
        }

        public async Task<bool> DeleteFileOrFolder(FileDetail Detail)
        {
            try
            {
                MapFolder[Detail.FileName] = null;
                await this.graphClient.Drive.Items[Detail.Id].Request().DeleteAsync();
                return true;
            }
            catch (Exception ex)
            {
                ShowDialogException(ex);
                return false;
            }
        }

        public FileDetail GetInnerFolder()
        {
            if(this.InnerFolder != null)
            {
                FileDetail fileDetail = new FileDetail(this.InnerFolder.Id, this.InnerFolder.Name,(long) this.InnerFolder.Size, this.InnerFolder.Folder != null);
                return fileDetail;
            }
            return null;
        }

        public FileDetail GetCurrentFolder()
        {
            if (this.CurrentFolder != null)
            {
                FileDetail fileDetail = new FileDetail(this.CurrentFolder.Id, this.CurrentFolder.Name, (long)this.CurrentFolder.Size, this.CurrentFolder.Folder != null);
                return fileDetail;
            }
            return null;
        }

        public async Task<FileDetail> CreateFolder(string FolderName, FileDetail Where)
        {
            try
            {
                var folderToCreate = new DriveItem { Name = FolderName, Folder = new Folder() };
                var newFolder = await graphClient.Drive.Items[Where.Id].Children.Request().AddAsync(folderToCreate);
                if (newFolder != null)
                {
                    MapFolder.Add(newFolder.Name, newFolder);
                    FileDetail fileDetail = new FileDetail(newFolder.Id, newFolder.Name,(long) newFolder.Size, newFolder.Folder != null);
                    return fileDetail;
                }
            }
            catch (Exception ex)
            {
                ShowDialogException(ex);
            }
            return null;
        }

        public async Task<bool> DownloadFile(FileDetail fileDetail,string FileNameSaveDisk)
        {
            try
            {
                using (var stream = await this.graphClient.Drive.Items[fileDetail.Id].Content.Request().GetAsync())
                using (var outputStream = new System.IO.FileStream(FileNameSaveDisk, System.IO.FileMode.Create))
                {
                    await stream.CopyToAsync(outputStream);
                }
                return true;
            }
            catch (Exception ex)
            {
                ShowDialogException(ex);
                return false;
            }
        }

        public async Task<FileDetail> UploadFile()
        {
            try
            {
                if (this.CurrentFolder != null)
                {
                    string filename;
                    using (var stream = this.GetFileStreamForUpload(this.CurrentFolder.Name, out filename))
                    {
                        if (stream != null)
                        {
                            try
                            {
                                var uploadedItem = await this.graphClient.Drive.Items[this.CurrentFolder.Id].ItemWithPath(filename).Content.Request().PutAsync<DriveItem>(stream);
                                MapFolder.Add(uploadedItem.Name, uploadedItem);
                                FileDetail fileDetail = new FileDetail(uploadedItem.Id, uploadedItem.Name, (long)uploadedItem.Size, uploadedItem.Folder != null);
                                return fileDetail;
                            }
                            catch (Exception exception)
                            {
                                this.ShowDialogException(exception);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ShowDialogException(ex);
            }
            return null;
        }

        public long GetTotalFilesCurrent()
        {
            return MapFolder.Count;
        }

        public abstract void ShowDialogException(Exception exception);

        public abstract DialogResult MessageBox(string message, bool enableButtonCancel = false);

        #endregion

        #region private_methods

        private async Task<bool> LoadFolderFromPathOrId(string pathOrId = null, bool usingId = false)
        {
            if (this.graphClient == null)
                return false;
            LoadChildren(new DriveItem[0]);
            try
            {
                DriveItem folder;
                if (pathOrId == null)
                {
                    folder = await this.graphClient.Drive.Root.Request().Expand(expandString).GetAsync();
                }
                else if (usingId)
                {
                    folder =
                        await
                            this.graphClient.Drive.Items[pathOrId]
                                .Request()
                                .Expand(expandString)
                                .GetAsync();
                }
                else
                {

                    folder =
                        await
                            this.graphClient.Drive.Root.ItemWithPath("/" + pathOrId)
                                .Request()
                                .Expand(expandString)
                                .GetAsync();
                }
                ProcessFolder(folder);
            }
            catch (Exception exception)
            {
                if (this.IsLogged)
                    ShowDialogException(exception);
                return false;
            }
            return true;
        }

        private void ProcessFolder(DriveItem folder)
        {
            if (folder != null)
            {
                if (folder.Folder != null && folder.Children != null && folder.Children.CurrentPage != null)
                {
                    if (folder.Name != "root")
                        InnerFolder = folder;
                    this.CurrentFolder = folder;
                    LoadChildren(folder.Children.CurrentPage);
                }
            }
        }

        private void LoadChildren(IList<DriveItem> items)
        {
            MapFolder.Clear();
            foreach (DriveItem item in items)
            {
                MapFolder.Add(item.Name, item);
            }
        }

        private async Task<string> GetTokenForUserAsync()
        {
            AuthenticationResult authResult;
            try
            {
                authResult = await IdentityClientApp.AcquireTokenSilentAsync(Scopes);
                TokenForUser = authResult.Token;
            }

            catch (Exception)
            {
                if (TokenForUser == null || Expiration <= DateTimeOffset.UtcNow.AddMinutes(5))
                {
                    authResult = await IdentityClientApp.AcquireTokenAsync(Scopes);

                    TokenForUser = authResult.Token;
                    Expiration = authResult.ExpiresOn;
                }
            }

            return TokenForUser;
        }

        private Stream GetFileStreamForUpload(string targetFolderName, out string originalFilename)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Upload to " + targetFolderName;
            dialog.Filter = "All Files (*.*)|*.*";
            dialog.CheckFileExists = true;
            var response = dialog.ShowDialog();
            if (response != DialogResult.OK)
            {
                originalFilename = null;
                return null;
            }

            try
            {
                originalFilename = System.IO.Path.GetFileName(dialog.FileName);
                return new System.IO.FileStream(dialog.FileName, System.IO.FileMode.Open,FileAccess.Read);
            }
            catch (Exception ex)
            {
                MessageBox("Error uploading file: " + ex.Message);
                originalFilename = null;
                return null;
            }
        }

        #endregion
    }
}
