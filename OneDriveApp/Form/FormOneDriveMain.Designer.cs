﻿namespace OneDriveApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "someFile.txt",
            "1 KB"}, -1);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnLoginLogout = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnDownload = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnUpload = new MaterialSkin.Controls.MaterialFlatButton();
            this.materialDividerButtons = new MaterialSkin.Controls.MaterialDivider();
            this.btnNewFolder = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnDeleteItem = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnRoot = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnInnerFolder = new MaterialSkin.Controls.MaterialFlatButton();
            this.lblFolder = new MaterialSkin.Controls.MaterialFlatButton();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lstViewItems = new MaterialSkin.Controls.MaterialListView();
            this.picOneDrive = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picOneDrive)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLoginLogout
            // 
            this.btnLoginLogout.AutoSize = true;
            this.btnLoginLogout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnLoginLogout.Depth = 0;
            this.btnLoginLogout.Icon = null;
            this.btnLoginLogout.Location = new System.Drawing.Point(13, 77);
            this.btnLoginLogout.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnLoginLogout.MinimumSize = new System.Drawing.Size(75, 0);
            this.btnLoginLogout.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnLoginLogout.Name = "btnLoginLogout";
            this.btnLoginLogout.Primary = false;
            this.btnLoginLogout.Size = new System.Drawing.Size(75, 36);
            this.btnLoginLogout.TabIndex = 2;
            this.btnLoginLogout.Text = "Login";
            this.btnLoginLogout.UseVisualStyleBackColor = true;
            this.btnLoginLogout.Click += new System.EventHandler(this.BtnLoginLogout_Click);
            // 
            // btnDownload
            // 
            this.btnDownload.AutoSize = true;
            this.btnDownload.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnDownload.BackColor = System.Drawing.Color.Transparent;
            this.btnDownload.Depth = 0;
            this.btnDownload.ForeColor = System.Drawing.Color.Black;
            this.btnDownload.Icon = null;
            this.btnDownload.Location = new System.Drawing.Point(342, 89);
            this.btnDownload.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnDownload.MinimumSize = new System.Drawing.Size(96, 36);
            this.btnDownload.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Primary = false;
            this.btnDownload.Size = new System.Drawing.Size(96, 36);
            this.btnDownload.TabIndex = 3;
            this.btnDownload.Text = "Download";
            this.btnDownload.UseVisualStyleBackColor = true;
            this.btnDownload.Visible = false;
            this.btnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // btnUpload
            // 
            this.btnUpload.AutoSize = true;
            this.btnUpload.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUpload.Depth = 0;
            this.btnUpload.Icon = null;
            this.btnUpload.Location = new System.Drawing.Point(238, 89);
            this.btnUpload.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnUpload.MinimumSize = new System.Drawing.Size(96, 36);
            this.btnUpload.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Primary = false;
            this.btnUpload.Size = new System.Drawing.Size(96, 36);
            this.btnUpload.TabIndex = 4;
            this.btnUpload.Text = "Upload";
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Visible = false;
            this.btnUpload.Click += new System.EventHandler(this.BtnUpload_Click);
            // 
            // materialDividerButtons
            // 
            this.materialDividerButtons.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialDividerButtons.Depth = 0;
            this.materialDividerButtons.Location = new System.Drawing.Point(123, 77);
            this.materialDividerButtons.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDividerButtons.Name = "materialDividerButtons";
            this.materialDividerButtons.Size = new System.Drawing.Size(429, 117);
            this.materialDividerButtons.TabIndex = 5;
            this.materialDividerButtons.Text = "materialDivider1";
            this.materialDividerButtons.Visible = false;
            // 
            // btnNewFolder
            // 
            this.btnNewFolder.AutoSize = true;
            this.btnNewFolder.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnNewFolder.Depth = 0;
            this.btnNewFolder.Icon = null;
            this.btnNewFolder.Location = new System.Drawing.Point(134, 89);
            this.btnNewFolder.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnNewFolder.MaximumSize = new System.Drawing.Size(96, 36);
            this.btnNewFolder.MinimumSize = new System.Drawing.Size(96, 36);
            this.btnNewFolder.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnNewFolder.Name = "btnNewFolder";
            this.btnNewFolder.Primary = false;
            this.btnNewFolder.Size = new System.Drawing.Size(96, 36);
            this.btnNewFolder.TabIndex = 6;
            this.btnNewFolder.Text = "New folder";
            this.btnNewFolder.UseVisualStyleBackColor = true;
            this.btnNewFolder.Visible = false;
            this.btnNewFolder.Click += new System.EventHandler(this.BtnNewFolder_Click);
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.AutoSize = true;
            this.btnDeleteItem.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnDeleteItem.Depth = 0;
            this.btnDeleteItem.Icon = null;
            this.btnDeleteItem.Location = new System.Drawing.Point(446, 89);
            this.btnDeleteItem.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnDeleteItem.MaximumSize = new System.Drawing.Size(96, 36);
            this.btnDeleteItem.MinimumSize = new System.Drawing.Size(96, 36);
            this.btnDeleteItem.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.Primary = false;
            this.btnDeleteItem.Size = new System.Drawing.Size(96, 36);
            this.btnDeleteItem.TabIndex = 7;
            this.btnDeleteItem.Text = "Delete Item";
            this.btnDeleteItem.UseVisualStyleBackColor = true;
            this.btnDeleteItem.Visible = false;
            this.btnDeleteItem.Click += new System.EventHandler(this.BtnDeleteItem_Click);
            // 
            // btnRoot
            // 
            this.btnRoot.AutoSize = true;
            this.btnRoot.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnRoot.Depth = 0;
            this.btnRoot.Icon = null;
            this.btnRoot.Location = new System.Drawing.Point(238, 144);
            this.btnRoot.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnRoot.MinimumSize = new System.Drawing.Size(96, 36);
            this.btnRoot.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnRoot.Name = "btnRoot";
            this.btnRoot.Primary = false;
            this.btnRoot.Size = new System.Drawing.Size(96, 36);
            this.btnRoot.TabIndex = 10;
            this.btnRoot.Text = "/";
            this.btnRoot.UseVisualStyleBackColor = true;
            this.btnRoot.Visible = false;
            this.btnRoot.Click += new System.EventHandler(this.BtnRoot_Click);
            // 
            // btnInnerFolder
            // 
            this.btnInnerFolder.AutoSize = true;
            this.btnInnerFolder.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnInnerFolder.Depth = 0;
            this.btnInnerFolder.Icon = null;
            this.btnInnerFolder.Location = new System.Drawing.Point(342, 144);
            this.btnInnerFolder.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnInnerFolder.MinimumSize = new System.Drawing.Size(200, 36);
            this.btnInnerFolder.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnInnerFolder.Name = "btnInnerFolder";
            this.btnInnerFolder.Primary = false;
            this.btnInnerFolder.Size = new System.Drawing.Size(200, 36);
            this.btnInnerFolder.TabIndex = 11;
            this.btnInnerFolder.UseVisualStyleBackColor = true;
            this.btnInnerFolder.Visible = false;
            this.btnInnerFolder.Click += new System.EventHandler(this.BtnInnerFolder_Click);
            // 
            // lblFolder
            // 
            this.lblFolder.AutoSize = true;
            this.lblFolder.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.lblFolder.Depth = 0;
            this.lblFolder.Icon = null;
            this.lblFolder.Location = new System.Drawing.Point(134, 144);
            this.lblFolder.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.lblFolder.MinimumSize = new System.Drawing.Size(96, 36);
            this.lblFolder.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblFolder.Name = "lblFolder";
            this.lblFolder.Primary = false;
            this.lblFolder.Size = new System.Drawing.Size(96, 36);
            this.lblFolder.TabIndex = 12;
            this.lblFolder.Text = "Navigate:";
            this.lblFolder.UseVisualStyleBackColor = true;
            this.lblFolder.Visible = false;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "File";
            this.columnHeader1.Width = 433;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Size";
            this.columnHeader2.Width = 107;
            // 
            // lstViewItems
            // 
            this.lstViewItems.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstViewItems.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lstViewItems.Depth = 0;
            this.lstViewItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.lstViewItems.FullRowSelect = true;
            this.lstViewItems.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            listViewItem1.StateImageIndex = 0;
            this.lstViewItems.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1});
            this.lstViewItems.Location = new System.Drawing.Point(12, 210);
            this.lstViewItems.MaximumSize = new System.Drawing.Size(540, 600);
            this.lstViewItems.MinimumSize = new System.Drawing.Size(540, 600);
            this.lstViewItems.MouseLocation = new System.Drawing.Point(-1, -1);
            this.lstViewItems.MouseState = MaterialSkin.MouseState.OUT;
            this.lstViewItems.Name = "lstViewItems";
            this.lstViewItems.OwnerDraw = true;
            this.lstViewItems.Size = new System.Drawing.Size(540, 600);
            this.lstViewItems.TabIndex = 1;
            this.lstViewItems.UseCompatibleStateImageBehavior = false;
            this.lstViewItems.View = System.Windows.Forms.View.Details;
            this.lstViewItems.Visible = false;
            this.lstViewItems.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.LstViewItems_MouseDoubleClick);
            // 
            // picOneDrive
            // 
            this.picOneDrive.ErrorImage = null;
            this.picOneDrive.Image = global::OneDriveApp.Properties.Resources.onedrive;
            this.picOneDrive.Location = new System.Drawing.Point(12, 122);
            this.picOneDrive.Name = "picOneDrive";
            this.picOneDrive.Size = new System.Drawing.Size(75, 72);
            this.picOneDrive.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picOneDrive.TabIndex = 13;
            this.picOneDrive.TabStop = false;
            this.picOneDrive.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 828);
            this.Controls.Add(this.picOneDrive);
            this.Controls.Add(this.lblFolder);
            this.Controls.Add(this.btnInnerFolder);
            this.Controls.Add(this.btnRoot);
            this.Controls.Add(this.btnDeleteItem);
            this.Controls.Add(this.btnNewFolder);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.btnDownload);
            this.Controls.Add(this.btnLoginLogout);
            this.Controls.Add(this.lstViewItems);
            this.Controls.Add(this.materialDividerButtons);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Sizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "One Drive App";
            ((System.ComponentModel.ISupportInitialize)(this.picOneDrive)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MaterialSkin.Controls.MaterialFlatButton btnLoginLogout;
        private MaterialSkin.Controls.MaterialFlatButton btnDownload;
        private MaterialSkin.Controls.MaterialFlatButton btnUpload;
        private MaterialSkin.Controls.MaterialDivider materialDividerButtons;
        private MaterialSkin.Controls.MaterialFlatButton btnNewFolder;
        private MaterialSkin.Controls.MaterialFlatButton btnDeleteItem;
        private MaterialSkin.Controls.MaterialFlatButton btnRoot;
        private MaterialSkin.Controls.MaterialFlatButton btnInnerFolder;
        private MaterialSkin.Controls.MaterialFlatButton lblFolder;
        private System.Windows.Forms.PictureBox picOneDrive;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private MaterialSkin.Controls.MaterialListView lstViewItems;
    }
}

