﻿using MaterialSkin;
using OneDriveApp.Class;
using OneDriveApp.Form;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace OneDriveApp
{
    public partial class Form1 : MaterialSkin.Controls.MaterialForm, IException
    {
        #region private_variables

        private OneDriveConnect oneDriveConnect;
        private const string MsaClientId = "12cb44b2-5811-4ea6-ae7f-89197d0b0e00";
        private readonly string[] Scopes = { "Files.ReadWrite.All" };

        #endregion

        #region public_methods

        public Form1()
        {
            InitializeComponent();
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
        }

        #endregion

        #region private_methods

        private class OneDriveConnect : IOneDriveConnect
        {
            private IException Iexception;
            public OneDriveConnect(IException Iexception,string MsaClientId, string[] scopes) : base(MsaClientId, scopes)
            {
                this.Iexception = Iexception;
            }

            public override bool Equals(object obj)
            {
                return base.Equals(obj);
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            public override DialogResult MessageBox(string message, bool enableButtonCancel)
            {
                return this.Iexception.MessageBox(message, enableButtonCancel);
            }

            public override void ShowDialogException(Exception exception)
            {
                string message = exception.Message;
                MessageBox(string.Format("OneDrive reported the following error: {0}", message),false);
            }

            public override string ToString()
            {
                return base.ToString();
            }
        }

        private async void BtnLoginLogout_Click(object sender, EventArgs e)
        {
            if (btnLoginLogout.Text == "Login")
            {
                oneDriveConnect = new OneDriveConnect(this,MsaClientId, Scopes);
                if (await this.oneDriveConnect.SignIn())
                {
                    PopulateListViewFromFolder(null);
                    var name = oneDriveConnect.GetConnectedNameUser();
                    if (name != null)
                        UpdateOptionsVisible(true, name);
                    else
                        UpdateOptionsVisible(true);
                }
                else
                {
                    UpdateOptionsVisible(false);
                }
            }
            else
            {
                if(oneDriveConnect != null)
                    oneDriveConnect.SignOut();
                UpdateOptionsVisible(false);
                oneDriveConnect = null;
            }
        }

        private void UpdateOptionsVisible(bool value,string name = null)
        {
            btnDownload.Visible = value;
            btnUpload.Visible = value;
            btnNewFolder.Visible = value;
            btnDeleteItem.Visible = value;
            materialDividerButtons.Visible = value;
            lblFolder.Visible = value;
            btnRoot.Visible = value;
            btnInnerFolder.Visible = value;
            lstViewItems.Visible = value;
            picOneDrive.Visible = value;
            if (value)
            {
                btnLoginLogout.Text = "Logout";
                this.Text = "OneDrive  (" + (name != null ? name : "?") + ")";
            }
            else
            {
                btnLoginLogout.Text = "Login";
                this.Text = "OneDrive";
            }
        }

        private async void BtnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                if (oneDriveConnect.GetTotalFilesCurrent() == 0 || lstViewItems.SelectedItems.Count == 0)
                {
                    MessageBox("There is no item selected to download!");
                }
                else
                {
                    if (lstViewItems.SelectedItems.Count > 1)
                    {
                        bool goAhead = true;
                        foreach (ListViewItem itemList in lstViewItems.SelectedItems)
                        {
                            var name = itemList.Name;
                            var item = this.GetFolderOrFileDetail(name);
                            if(item == null)
                            {
                                MessageBox(string.Format("Item \"{0}\" Not Found!\n can not be downloaded!", name));
                                goAhead = false;
                                break;
                            }
                            if (item.IsFolder == true)
                            {
                                MessageBox(string.Format("Folder\n\"{0}\"\n can not be downloaded!", name));
                                goAhead = false;
                                break;
                            }
                        }
                        if (goAhead)
                        {
                            var dialog = new FolderBrowserDialog
                            {
                                Description = "Select the folder to download all items"
                            };
                            var result = dialog.ShowDialog();
                            if (result == System.Windows.Forms.DialogResult.OK)
                            {
                                bool sucess = true;
                                foreach (ListViewItem itemList in lstViewItems.SelectedItems)
                                {
                                    var name = itemList.Name;
                                    var item = this.GetFolderOrFileDetail(name);
                                    if (item != null)
                                    {
                                        string fileName = dialog.SelectedPath + "\\" + name;
                                        if (await this.oneDriveConnect.DownloadFile(item, fileName) == false)
                                        {
                                            sucess = false;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        MessageBox("Unexpected item not found:\n" + name);
                                        sucess = false;
                                    }
                                }
                                if (sucess)
                                    Process.Start("explorer.exe", dialog.SelectedPath);
                            }
                        }
                    }
                    else
                    {
                        var name = lstViewItems.SelectedItems[0].Name;
                        var item = this.GetFolderOrFileDetail(name);
                        if (item != null)
                        {
                            if (item.IsFolder == true)
                            {
                                MessageBox(string.Format("Folder\n\"{0}\"\n can not be downloaded!", name));
                            }
                            else
                            {
                                var dialog = new SaveFileDialog
                                {
                                    FileName = item.FileName,
                                    Filter = "All Files (*.*)|*.*"
                                };
                                var result = dialog.ShowDialog();
                                if (result == System.Windows.Forms.DialogResult.OK)
                                {
                                    string fileName = dialog.FileName;
                                    if (await this.oneDriveConnect.DownloadFile(item, fileName) == true)
                                    {
                                        string path = Path.GetDirectoryName(fileName);
                                        Process.Start("explorer.exe", path);
                                    }
                                }
                            }
                        }
                        else
                        {
                            MessageBox("Unexpected item not found:\n" + name);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox(exception.ToString(),false);
            }
        }

        private async void BtnUpload_Click(object sender, EventArgs e)
        {
            var infoFile = await oneDriveConnect.UploadFile();
            if (infoFile != null)
            {
                this.AddItemToListView(infoFile);
                MessageBox(string.Format("Successfully uploaded file:\n{0}\n\nSize:{1}", infoFile.FileName, infoFile.GetHumanSizeFile()));
            }
        }

        private async void BtnNewFolder_Click(object sender, EventArgs e)
        {
            var CurrentFolder = oneDriveConnect.GetCurrentFolder();
            if (CurrentFolder != null)
            {
                string Where = CurrentFolder.FileName != "root" ? CurrentFolder.FileName : "/";
                FormInputDialog dialog = new FormInputDialog(Where);
                var result = dialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrEmpty(dialog.InputFolder))
                {
                    try
                    {
                        var FolderName = dialog.InputFolder;
                        var InfoFolder = await oneDriveConnect.CreateFolder(FolderName, CurrentFolder);
                        if (InfoFolder != null)
                        {
                            AddItemToListView(InfoFolder);
                            MessageBox("Successfully folder created:\n" + FolderName);
                        }
                    }
                    catch (Exception exception)
                    {
                        MessageBox(exception.ToString(), false);
                    }
                }
            }
        }

        private void LstViewItems_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                MaterialSkin.Controls.MaterialListView items = sender as MaterialSkin.Controls.MaterialListView;
                var item = items.SelectedItems[0];
                var folder = this.GetFolderOrFileDetail(item.Name);
                if (folder != null && folder.IsFolder == true)
                {
                    PopulateListViewFromFolder(folder);
                }
            }
            catch (Exception exception)
            {
                MessageBox(exception.ToString(), false);
            }
        }

        private async void BtnDeleteItem_Click(object sender, EventArgs e)
        {
            if (oneDriveConnect.GetTotalFilesCurrent() == 0 || lstViewItems.SelectedItems.Count == 0)
            {
                MessageBox("There is no item selected\nto be delete!.",false);
            }
            else
            {
                var CurrentFolder = this.oneDriveConnect.GetCurrentFolder();
                if (lstViewItems.SelectedItems.Count > 1)
                {
                    string allItemsAsString= "";
                    for(int i=0; i< lstViewItems.SelectedItems.Count; ++i)
                    {
                        var itemList = lstViewItems.SelectedItems[i];
                        allItemsAsString += itemList.Name;
                        int iNext = i + 1;
                        if (iNext > 3)
                        {
                            allItemsAsString += "\n...";
                            break;
                        }
                        else if (iNext < lstViewItems.SelectedItems.Count)
                        {
                            allItemsAsString += "\n";
                        }
                    }
                    
                    var result = MessageBox(string.Format("Are you sure you want to delete those {0} items?\n{1}", lstViewItems.SelectedItems.Count, allItemsAsString), true);
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        try
                        {
                            for (int i = 0; i < lstViewItems.SelectedItems.Count; ++i)
                            {
                                var itemList = lstViewItems.SelectedItems[i];
                                var itemToDelete = this.GetFolderOrFileDetail(itemList.Name);
                                if(await  oneDriveConnect.DeleteFileOrFolder(itemToDelete))
                                {
                                    if (this.btnInnerFolder.Text == itemToDelete.FileName)
                                        this.btnInnerFolder.Text = "";
                                }
                            }
                            int iTotalDeleted = lstViewItems.SelectedItems.Count;
                            while (lstViewItems.SelectedItems.Count > 0)
                            {
                                var itemList = lstViewItems.SelectedItems[0];
                                lstViewItems.Items.Remove(itemList);
                            }
                            MessageBox(string.Format("{0} Items \ndeleted successfully!", iTotalDeleted));
                        }
                        catch (Exception exception)
                        {
                            MessageBox(exception.ToString(), false);
                        }
                    }
                }
                else
                {
                    var itemList = lstViewItems.SelectedItems[0];
                    var itemToDelete = this.GetFolderOrFileDetail(itemList.Name);
                    string itemName = itemToDelete.FileName;
                    var result = MessageBox("Are you sure you want to delete this item?\n\n" + itemName, true);
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        try
                        {
                            if (await oneDriveConnect.DeleteFileOrFolder(itemToDelete))
                            {
                                lstViewItems.Items.Remove(itemList);
                                if (this.btnInnerFolder.Text == itemToDelete.FileName)
                                    this.btnInnerFolder.Text = "";
                                MessageBox(string.Format("Item \n{0}\ndeleted successfully!", itemName));
                            }
                            
                        }
                        catch (Exception exception)
                        {
                            MessageBox(exception.ToString(), false);
                        }
                    }
                }
            }
        }

        private void BtnRoot_Click(object sender, EventArgs e)
        {
            var CurrentFolder = oneDriveConnect.GetCurrentFolder();
            if (CurrentFolder != null && CurrentFolder.FileName != "root")
            {
                PopulateListViewFromFolder(null);
            }
        }

        private void BtnInnerFolder_Click(object sender, EventArgs e)
        {
            if (this.btnInnerFolder.Text.Length > 0)
            {
                var CurrentFolder = oneDriveConnect.GetCurrentFolder();
                var CurrentInner = oneDriveConnect.GetInnerFolder();
                if (CurrentFolder != null && CurrentInner != null && CurrentFolder.FileName != CurrentInner.FileName)
                {
                    PopulateListViewFromFolder(CurrentInner);
                }
            }
        }

        private void AddItemToListView(OneDriveConnect.FileDetail InfoFile)
        {
            var FileName = InfoFile.FileName;
            if (FileName.Length > 58)
                FileName = FileName.Substring(0, 55) + " ...";
            if (InfoFile.IsFolder)
                FileName = "/" + FileName;
            var list = lstViewItems.Items.Add(FileName);
            list.Name = FileName;
            var size = InfoFile.GetHumanSizeFile();
            list.SubItems.Add(size);
        }

        private async void PopulateListViewFromFolder(OneDriveConnect.FileDetail WhichFolder)
        {
            var Files = await this.oneDriveConnect.ListFilesOnFolder(WhichFolder);
            lstViewItems.Items.Clear();
            if (Files != null)
            {
                foreach (OneDriveConnect.FileDetail InfoFile in Files)
                {
                    AddItemToListView(InfoFile);
                }
                if (WhichFolder != null && WhichFolder.FileName != "root")
                {
                    if (WhichFolder.FileName.Length >= 19)
                        btnInnerFolder.Text = WhichFolder.FileName.Substring(0, 19) + " ...";
                    else
                        btnInnerFolder.Text = WhichFolder.FileName;
                }
            }
            lstViewItems.Refresh();
            AdjustListSize();
        }

        private OneDriveConnect.FileDetail GetFolderOrFileDetail(string NameInCurrentFolder)
        {
            if (NameInCurrentFolder != null && NameInCurrentFolder.Substring(0, 1) == "/")
                NameInCurrentFolder = NameInCurrentFolder.Substring(1);
            var Item = oneDriveConnect.GetDriveItem(NameInCurrentFolder);
            return Item;
        }

        private void AdjustListSize()
        {
            if (lstViewItems.Columns.Count >= 2)
                lstViewItems.Columns[0].Width = Width - 32 - SystemInformation.VerticalScrollBarWidth - lstViewItems.Columns[1].Width;
        }

        public DialogResult MessageBox(string message, bool enableButtonCancel = false)
        {
            FormMessageBox dialog = new FormMessageBox(message, enableButtonCancel);
            var response = dialog.ShowDialog();
            return response;
        }

        public void ShowDialogException(Exception exception)
        {
            FormMessageBox dialog = new FormMessageBox(exception.Message, false);
            dialog.ShowDialog();
        }

        #endregion
    }
}
